const getComponentInstance = (selector) =>   document.querySelector(selector);
const getComponentAll = (selector) =>   document.querySelectorAll(selector);
const formatNumber = (number) => {
  return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(number);
}

// quantidade de produtos definida no modal
let modalQtde = 1;

// produto que foi clicado
let productKey = 0;

// carinho de compras em memória
let cart = [];

// fecha a modal no evento click do seletor passado por parâmetro
const closeModal = (seletor = '') => {
  if(seletor != '') {
    document.querySelector(seletor).addEventListener('click', (event) => {
      let modal = getComponentInstance('.pizzaWindowArea');
      modal.style.opacity = 0;
      setTimeout(() => {
        modal.style.display = 'none';
      }, 200);
    })
  } else {
    let modal = getComponentInstance('.pizzaWindowArea');
      modal.style.opacity = 0;
      setTimeout(() => {
        modal.style.display = 'none';
      }, 200);
  }  
}

pizzaJson.map((item, index) => {
  // clona o HTML do card que será replicado na página
  let pizzaItem = getComponentInstance('.models .pizza-item').cloneNode(true);
  
  // adiciona atributo identificador no item card
  pizzaItem.setAttribute('data-key', index);

  // carrega os dados do JSON no item card
  pizzaItem.querySelector('.pizza-item--img img').src = item.img;
  pizzaItem.querySelector('.pizza-item--name').innerHTML = item.name;
  pizzaItem.querySelector('.pizza-item--price').innerHTML = formatNumber(item.price.toFixed(2));
  pizzaItem.querySelector('.pizza-item--desc').innerHTML = item.description;
  pizzaItem.querySelector('a').addEventListener('click', (event) => {
    event.preventDefault();

    // reseta a quantidade sempre que o modal é aberto
    modalQtde = 1;

    //recupera o data-key do item clicado
    let key = event.target.closest('.pizza-item').getAttribute('data-key');
    productKey = key;

    // obtem o HTML do modal
    let modal = getComponentInstance('.pizzaWindowArea');
    document.querySelector('.pizzaInfo--qt').innerHTML = modalQtde;

    // exibe o modal na página com transitions
    modal.style.opacity = 0;
    modal.style.display = 'flex';
    setTimeout(() => {
      modal.style.opacity = 1;
    }, 200);

    //carrega os dados do item clicada no modal
    modal.querySelector('.pizzaBig img').src = pizzaJson[key].img;
    modal.querySelector('.pizzaInfo--desc').innerHTML = pizzaJson[key].description;
    modal.querySelector('.pizzaInfo h1').innerHTML = pizzaJson[key].name;
    modal.querySelector('.pizzaInfo--actualPrice').innerHTML = formatNumber(pizzaJson[key].price.toFixed(2));

    // reseta o tamanho selecionado
    document.querySelector('.pizzaInfo--size.selected').classList.remove('selected');

    document.querySelectorAll('.pizzaInfo--size').forEach((size, sizeIndex) => {
      if(sizeIndex === 2) {
        size.classList.add('selected');
      }     

      // carrega os tamanhos disponíveis
      size.querySelector('span').innerHTML = pizzaJson[key].sizes[sizeIndex];      
    });
  })  
  
  // adiciona o card com os dados do produto populados
  getComponentInstance('.pizza-area').append(pizzaItem);
});

// fecha o modal ao clicar no botão cancelar
closeModal('.pizzaInfo--cancelButton');
closeModal('.pizzaInfo--cancelMobileButton'); 

// ação de DIMINUIR a quantidade no modal
document.querySelector('.pizzaInfo--qtmenos').addEventListener('click', () => {
  if(modalQtde > 1) {
    modalQtde--;
    document.querySelector('.pizzaInfo--qt').innerHTML = modalQtde;
  }
});

// ação de AUMENTAR a quantidade no modal
document.querySelector('.pizzaInfo--qtmais').addEventListener('click', () => {
  modalQtde++;
  document.querySelector('.pizzaInfo--qt').innerHTML = modalQtde;
});

// ação de alterar a seleção do TAMANHO
document.querySelectorAll('.pizzaInfo--size').forEach(item =>{
  item.addEventListener('click', (e) => {
    document.querySelector('.selected').classList.remove('selected');
    item.classList.add('selected');
  });
});

// ação de adicionar o item ao carrinho de compras
document.querySelector('.pizzaInfo--addButton').addEventListener('click', (event) => {
  let qtde = parseInt(document.querySelector('.pizzaWindowArea').querySelector('.pizzaInfo--qt').innerHTML);
  let size = parseInt(document.querySelector('.pizzaInfo--size.selected').getAttribute('data-key'));
  const price = parseFloat(pizzaJson[productKey].price, 2);
  const productId = parseInt(pizzaJson[productKey].id);

  // cria identificador único para o item com id do produto e tamanho
  const itemId = productId+'@'+size;

  let indexProductFound = cart.findIndex((product) => product.itemId == itemId);
  // verifica se o item já existe
  if(indexProductFound > -1) {
    // se já existe incrementa a quantidade
    cart[indexProductFound].qtde += modalQtde;
  } else {
    // se não existe adiciona o item no carrinho
    cart.push({
      itemId,
      id: productId,
      qtde,
      size
    });
  }

  updateCart();
  closeModal();
});

// abre o carrinho de compras no mobile
document.querySelector('.menu-openner').addEventListener('click', (event) => {
  if(cart.length > 0){
    document.querySelector('aside').style.left = 0;
  }
});

// fecha o carrinho de compras no mobile
document.querySelector('.menu-closer').addEventListener('click', (event) => {
  document.querySelector('aside').style.left = '100vw';
});

const updateCart = () => {
  document.querySelector('.menu-openner span').innerHTML = cart.length;

  if(cart && cart.length > 0) {
    document.querySelector('.cart').innerHTML = '';
    let cartSubtotal = 0;
    let cartDesconto = 0;
    let cartTotal    = 0;

    for(let i in cart){   
      const pizzaItem = pizzaJson.find((item) => item.id == cart[i].id);
      let cartItem = document.querySelector('.models .cart--item').cloneNode(true);
      
      // alimenta o item com os dados
      let pizzaSizeName = '';
      switch (cart[i].size) {
        case 0:
          pizzaSizeName = 'P';
          break;

        case 1:
          pizzaSizeName = 'M';
          break;
        
        case 2:
          pizzaSizeName = 'G';
          break;
      }
      let pizzaName = `${ pizzaItem.name } (${ pizzaSizeName })`; 
       
      if(pizzaItem){
        cartItem.setAttribute('data-key', cart[i].itemId);
        cartItem.querySelector('.cart--item-nome').innerHTML = pizzaName;
        cartItem.querySelector('.cart--item--qt').innerHTML = cart[i].qtde;
        cartItem.querySelector('img').src = pizzaItem.img;
      } 

      // calcula os totais e desconto
      cartSubtotal += pizzaItem.price * cart[i].qtde;
      cartDesconto = parseFloat(cartSubtotal,2) * .1; // 10% de desconto
      cartTotal = parseFloat(cartSubtotal, 2) - parseFloat(cartDesconto, 2);

      let cartTotais = document.querySelector('.cart--details');
      cartTotais.querySelector('.cart--totalitem.subtotal span:last-child').innerHTML = formatNumber(cartSubtotal);
      cartTotais.querySelector('.cart--totalitem.desconto span:last-child').innerHTML = formatNumber(cartDesconto);
      cartTotais.querySelector('.cart--totalitem.total span:last-child').innerHTML = formatNumber(cartTotal);

      // diminui a quantidade e se chegar a zero remove o item do carrinho
      cartItem.querySelector('.cart--item-qtmenos').addEventListener('click', (event) => {
        if(cart[i].qtde > 1) {
          cart[i].qtde--;
        } else {
          cart.splice(i, 1);
        }
        updateCart();
      });

      cartItem.querySelector('.cart--item-qtmais').addEventListener('click', (event) => {
        cart[i].qtde++;        
        updateCart();
      });

      document.querySelector('.cart').append(cartItem);
    }         
  } 

  // exibe o carrinho rola a página para o topo com animação
  if(showCart()) {
    scrollToTop(1000);
  }
}

const showCart = () => {
  let asideCart = document.querySelector('aside');
  if(cart && cart.length > 0) {
    asideCart.classList.add('show');
    return true; 
  } else {
    asideCart.classList.remove('show');
    asideCart.style.left = '100vw';
    return false;
  }
}

const scrollToTop = (duration) => {
  // cancela se já estiver no topo
  if (document.scrollingElement.scrollTop === 0) return;

  const cosParameter = document.scrollingElement.scrollTop / 2;
  let scrollCount = 0, oldTimestamp = null;

  function step (newTimestamp) {
      if (oldTimestamp !== null) {
          // se duration for 0 scrollCount será infinito
          scrollCount += Math.PI * (newTimestamp - oldTimestamp) / duration;
          if (scrollCount >= Math.PI) return document.scrollingElement.scrollTop = 0;
          document.scrollingElement.scrollTop = cosParameter + cosParameter * Math.cos(scrollCount);
      }
      oldTimestamp = newTimestamp;
      window.requestAnimationFrame(step);
  }
  window.requestAnimationFrame(step);
}
